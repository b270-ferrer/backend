const express = require('express')
const mongoose = require('mongoose')

const app = express();
const port = 3000;
// Change password
/*
	MongoDB Atlas > Database Access (left navigation pane) >
	Lool for the admin uer > change password
*/
/*
	mongoose.connect - allows our application to be connected to MongoDB

	useNewUrlParser: true - allows us to avoid any current and future errors 
							while connecting to MOngo DB 

	useUnifiedTopology: true - it allow us to connect to MongoDB even if the 
								required IP address is updated
*/



//mongodb+srv://admin:(PASSWORD)@zuitt.5e2pgug.mongodb.net/(COLLECTION NAME)?retryWrites=true&w=majority
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5e2pgug.mongodb.net/b270-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);


//connection to the database
// set notification if the connection is a success or failure
const db = mongoose.connection;

// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in the browser 
//console as well as inthe terminal
// "connection error" is the message that will display if this happens
db.on("error", console.error.bind (console,"connection error"));

// if the connection is successful, output in the console "We are connected 
//the database"
db.once("open", () => console.log("We are connected to the database"));


// [Section] Mongoose Schema
// Schemas determine the structure of documents to be written in the database; they act as blueprints of our data
/*
Use the Schema() constructor of the mongoose module to create a new Schema object
*/
const taskSchema = new mongoose.Schema({
	// define the fields with corresponding data type
	// it needs a task "name" and task "status"
	// the "name" field requires a String data type for its value
	name : String,
	// "status" field requires a String data type, but since we have default : "pending", users can leave this blank with a default value of "pending"
	status : {
		type : String,
		default : "pending"
	}
})
// [Section] Models
// Models use Schemas and they act as the middleman from the server to our database Server > Schema (blueprint) > Database > Collection
// "Task" variable can now be used to run commands for interacting with our database; the naming convention for mongoose models follows the MVC format
// the first parameter of the model() indicates the collection to store the data that will be created
// the second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
const Task = mongoose.model("Task", taskSchema);

// [Section] Creation of todo list routes
// allows our app to read json data
app.use( express.json() );
// allows our app to read data from forms
app.use( express.urlencoded( { extended:true } ) )

// create a new task
// Business Logic
/*
	1. check if the task is already existing in the collection
		- if it exists, return an error/notice.
		- if it's not, we add it in the database
	2. the task data will be coming from the request body
	3. create a new Task object with a "name" field/property
	4. the "status" property does not need to be provided because our schema defaults it to "pending"
*/
app.post('/tasks', (req, res) => {
	// checking for the duplicate tasks
	Task.findOne({ name : req.body.name }).then((result, err)=>{
		// if it exists, return an error/notice.
		if (result != null && result.name === req.body.name){
			return res.send("Duplicate task found");
		// if no document was found
		}else{
			// create a new task and save it to the database
			let newTask = new Task({
				name : req.body.name
			})
			// save the object in the collection
			newTask.save().then((savedTask, error) => {
				// try-catch-finally can also be used for error handling
				if (error){
					return console.error(error)
				}else{
					return res.status(201).send("New Task Created")
				}
			})
		}
	})
})

// // Retrieve all the documents in the collection
// app.get('/tasks', async (req, res) => {
//   	try {
//     	const documents = await Task.find();
//     	res.status(200).send(documents);
//   	} 	catch (err) {
//     	console.error(err);
//     	res.status(500).send('Error retrieving documents');
//   	}
// });

app.get('/tasks', (req, res)=>{
	// retrieve all documents
	Task.find({  }).then((result, err)=>{
		// error handling
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})


// getting all tasks
/*
Business logic:
	1. Retrieve all the documents in the collection (find() method)
	2. if an error is encountered, print the error
	3. if no errors are found, send a success (200) status back to the client/postman and return the array of document/result
	solution:8:56 pm; you can send ss to our batch google chat
*/


//////////////////////// ACTIVITY //////////////////////////////

// 1. Create a User schema.
			const UserSchema = new mongoose.Schema({
				username : String,
				password : String
			});
// 2. Create a User model.
			const UserModel = mongoose.model('User', UserSchema );
// 3. Create a POST route that will access the "/signup" route that will create a user.
			app.post('/signup', async (req, res) => {
				let { username, password } = req.body;

			  // Create a new user object
			  const newUser = new User({
			    	username,
			    	password,
			  });

			  try {
			  		// Save the new user to the database
			    	const savedUser = await newUser.save();

			    	// Send a success response
			    	res.status(200).send(`User ${savedUser.username} created`);
			  	} catch (err) {
			    	console.error(err);
			    	res.status(500).send('Error creating user');
			  	}
			});
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Create a git repository named S35.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.
// NOTE: Continue the discussion codes












app.listen(port, () => console.log (`Server running at ${port}`))