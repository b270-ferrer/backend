// 
// ES 6 SCRIPTS

// ECMA Scripts 2015 updates to JavaScripts

// [section] ES6 Update
// ECMA- European Computer Manufacturer Association
// ECMAScript - the standard that is used to create implementation of the language, one which is JavaScript


//[section] Exponent Operator

// Exponent Operator
const anotherNum = 8 ** 2;
console.log(anotherNum)


// Math Object operator
const num = Math.pow(8,2);
console.log(num)



// [section] Template Literal

let name = "Daisy";

let message = "Hello " + name + "\n Welcome to Programming!"

console.log(message);

// String using template literal
// Uses backticks (``) instead of ("") or ('')

message = `Hello ${name}! Welcome to Programming!`

console.log(`Message with template literals: ${message}`);


// Multi line Using Template Literal

const anotherMessage = ` ${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${anotherNum}`

console.log(anotherMessage);

//  Template literal allow us to write strings  with embedded JavaScript expressions
//  -   ${}
// 

const interestRate = .5;
const principal = 1000;
console.log(`The interest on your savings account is: ${principal} * ${interestRate}`);

// [section] Array Destruction
// 
//  -   Allows us to unpact elements in arrays into distinct variables
//  -   Allows us to nmae array elements with variables instes of using index number
//  -   Syntax:
//      let/const [variableName1, variableName2, variableName3] = array
// 

const fullName = ["Monkey","Dragon", "Luffy"];

// Pre- array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
// Hello Monkey Dragon Luffy! It's nice to meet you!
console.log("Hello "+fullName[0]+" "+fullName[1]+" "+fullName[2]+"! It's nice to meet you!")

// Array Destructuring
const [firstName, middleName, lastName] = fullName
console.log(firstName)
console.log(middleName)
console.log(lastName)
console.log(fullName)
// Hello Monkey Dragon Luffy! It's nice to meet you!
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)


// [section] Object Destructuring

let person = {
    givenName: "Jane",
    midName: "Dela",
    familyName:"Cruz"
}

console.log(person.givenName);
console.log(person.midName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.midName} ${person.familyName}! Its good to see you`)

let {givenName, midName, familyName} = person;
console.log(`Hello ${givenName} ${midName} ${familyName}! Its good to see you`)

// [section] Arrow function
// 
//  - compact alternative to traditional function
//  - thiswill only work with "function expression"
// 
// -    Syntax:
//          let/const variableName = () =>{
//              code block/statement
//          }
// 

//function declaration
function greeting(){
    console.log("Hello World");
}
greeting();

//function expression
const greet = function(){
    console.log("Hi, World!")
}
//adding function inside another function
greet();
greet.play = function(){console.log("Play")}
greet.play();


//Arrow function

const greet1 = () => {
    console.log("Hello Again");
}

greet1();

const printFullName = (firstName, middleInitial, lastName) => {
    console.log (`${firstName} ${middleInitial} ${lastName}`)
}

printFullName("Monkey","D.","Luffy")


//Arrow function with loops

const student = ["John", "Jacob", "Jingleheimer", "Schmidt" ]

//Pre-arrow Function
student.forEach(
    function(student){
        console.log(`${student} is a student`);
    }
)


//Arrow Function
student.forEach( (student) => {
        console.log(`${student} is a student`);   
    } 
)

// const add = (x, y) => {
//     return x + y;
// }
// let total = add(1,2);
// console.log(total);     //3



// An arrow function has an implicit return value when the curly braces are OMMITED
const add = (x, y) =>  x + y;

let total = add(1,2);
console.log(total);     //3


// [section] Default function argument value
// Provides a default argument value if none is provided when the function is invoked

const greet2 = (name="User") => `Good Morning, ${name}`

console.log(greet2());
console.log(greet2("Zoie"));

//[section] Class Based Object Blueprints
// 
//  - the "constructor" is a special method of a class for creating/initializing an object for that class
//  - Syntax
//      class className {
//          constructor (objectPropertyA, objectPropertyB){
//              this.objectPropertyA = objectPropertyA;
//               this.objectPropertyB = objectPropertyB;
//          }
//      }
//  


class Car {
    constructor (brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}


const myCar = new Car();

console.log(myCar)

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar)

const myNewCar = new Car("Toyota","Fortuner",2020);
console.log(myNewCar)





















