// S24 Activity:
// 1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

let getCube = 2 ** 3;

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…

console.log (`The cube of 2 is ${getCube}`);

// 5. Create a variable address with a value of an array containing details of an address.

let address = ["258", "Washington Ave NW", "California", "90011"];

// 6. Destructure the array and print out a message with the full address using Template Literals.

let [houseNumber, street, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${street}, ${state} ${zipCode}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

let animal = {
    name: "Lolong",
    species: "saltwater crocodile",
    weight: "1075 Kgs",
    size: "20 ft 3 inches"
};


// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
let {name, species, weight, size} = animal;

console.log (`${name} was a ${species}. He weighed at ${weight} with a measurement of ${size}.`);

// 9. Create an array of numbers.

let numbers = [1,2,3,4,5];

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

numbers.forEach(
    (number) => console.log(number)
)

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduceNumber = numbers.reduce( (x,y) => x+y);
console.log (reduceNumber);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog {
    constructor (name,age,breed){
        this.name = name;
        this.age = age;
        this.breed =breed;
    }
};

// 13. Create/instantiate a new object from the class Dog and console log the object.

let dog = new Dog ("Frankie", 5, "Miiature Dachshund");
console.log(dog);

// 14. Create a git repository named S24.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.