 console.log("hello");
// 
// {SECTION] Arrays
// An Array inprogramming is simply a list of Data
// They are declared using the square brackets [] also known as "Array Literals"


let studentNumbers = ["2023-1923","2023-1924","2023-1925","2023-1926","2023-1927"];
console.log(studentNumbers)

let grades = [71,100,85,90];
console.log(grades)

let computerBrands = ["Acer","Lenovo","Asus","Apple","Huawei"];
console.log(computerBrands)

// Possible use of an array nut noy recommended
let mixedArray = [12,"Asus",null,undefined, {}];
console.log(mixedArray);

// Alternative wy of assigning array
let myTask = [
	"drink html",
	"eat java scrpt",
	"inhale css",
	"bake bootstrap"
];
console.log(myTask);
console.log(myTask.length);

// Create an array with values from variables
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Nairobi";
let city4 = "Rio";

let cities = [city1,city2,city3,city4];
console.log(cities);
console.log(cities.length);


let blankArray = []
console.log(blankArray.length);

let fullName = "Daisyrie Dela Cruz";
console.log (fullName )
console.log (fullName.length);


//  length property can set the total number of items in an array
// meaning we can delete the last item in an array by updating the length of the array
console.log (myTask )
console.log (myTask.length);
myTask.length = myTask.length-1;
console.log (myTask)
console.log (myTask.length);
myTask.length = myTask.length+1;
console.log (myTask)//last element is "empty"
console.log (myTask.length);

//another example using decrementation
console.log (cities);
console.log (cities.length);
cities.length --;
console.log (cities);
console.log (cities.length);
cities.length ++;
console.log (cities);//last element is "empty"
console.log (cities.length);


//Will not work with Strings
console.log (fullName);
console.log (fullName.length); //no change
fullName.length --;
console.log (fullName); 		//no change
console.log (fullName.length); //no change
fullName.length++;
console.log (fullName); 		//no change
console.log (fullName.length); //no change

// [SECTION Reading from Array]
console.log (computerBrands)
console.log (computerBrands[3])


console.log (grades)
console.log (grades[20]) //undefined


let lakersLegends = ["Kobe","Lebron","Shaq","Magic","Kareem"];
console.log (lakersLegends)
console.log (lakersLegends[0])//"Kobe",
console.log (lakersLegends[1])//"Lebron",
console.log (lakersLegends[2])//"Shaq",
console.log (lakersLegends[3])//"Magic",
console.log (lakersLegends[4])//"Kareem"

//  You can store array items inside another variables
currentLaker = lakersLegends[1];
console.log (currentLaker)//"Lebron",

console.log ("Array before reassignment")
console.log (lakersLegends)
lakersLegends[2] = "Davis";
console.log ("Array After reassignment")
console.log (lakersLegends)


// Accessing the last element of an array

let bullsLegends = ["Jordan","Pippen","Rodman","Rose","Kukoc"]
let lastElementIndex = bullsLegends.length-1;
console.log (bullsLegends)
console.log (bullsLegends[lastElementIndex])
console.log (bullsLegends[bullsLegends.length-1])


// Adding item into Array

let newArr = [];
console.log(newArr.length);
console.log(newArr[0]);//undefined

newArr[0] = "Cloud Strife";
console.log(newArr[0]);
console.log(newArr.length);

newArr[1] = "Tifa Lockhart";
console.log(newArr[1]);
console.log(newArr.length)


let numArr = [ 5, 10, 15, 20, 25, 2, 30, 40]

for (let index =0; index<numArr.length;index++){
	if (numArr[index] % 5 ===0){
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5")
	}


}

// MultiDimentional Array

let chessBoard = [
	["a1","b1","c1","d1","e1","f1","g1","h1"],
	["a2","b2","c2","d2","e2","f2","g2","h2"],
	["a3","b3","c3","d3","e3","f3","g3","h3"],
	["a4","b4","c4","d4","e4","f4","g4","h4"],
	["a5","b5","c5","d5","e5","f5","g5","h5"],
	["a6","b6","c6","d6","e6","f6","g6","h6"],
	["a7","b7","c7","d7","e7","f7","g7","h7"],
	["a8","b8","c8","d8","e8","f8","g8","h8"],
];
console.log(chessBoard);
//CONSOLE.TABLE
console.table(chessBoard);
//                     [row] [column]
console.log(chessBoard  [1]   [4]   ); //e2
console.log(chessBoard  [7]   [7]   ); //h8
console.log(chessBoard  [4]   [1]   ); //b5
console.log(chessBoard  [2]   [6]   ); //g3



















































