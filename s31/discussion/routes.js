const http = require('http');
// this creates a variable "port" to store port number
const port = 4000;


// creates a variable "server" that stores the output of the createServer method
const server = http.createServer((request, response) => {
		// accessing the /greetings route will return a message of "Hello World!"
	if (request.url == `/greeting`){

		response.writeHead(200, {'Content-Type': 'text/html'});
		//response.writeHead(404, {'Content-Type': 'text/plain'});

  		response.end('Hello, Batch 270!');

  		// accessing the /homepage route will return a message of "This is the homepage"
	}  else if (request.url == `/homepage`){

		response.writeHead(200, {'Content-Type': 'text/html'});

  		response.end('This is the homepage');

  		// all other routes will return a "404 page not available"
	}  else if (request.url == `/profile`){

		response.writeHead(200, {'Content-Type': 'text/html'});

  		response.end('This is the profile page');

  		// all other routes will return a "404 page not available"
	}else {

		response.writeHead(404, {'Content-Type': 'text/html'});

  		response.end('Page not available');

	}  


})

server.listen(port);

console.log (`Server is now accessible at localhost:${port}`);



























