/*
	Use "require" directive to load Node.JS module

	A modue is a software componet or part of a program 
	that contains one or more routine/ functions
	
	"HTTP" is a NODE JS module that let NODE JS transfer
	 data using Hyper Text Transfer Protocol

	Provides a funtionality for creating HTTP Servers and 
	Clients, allowing pplications to send and recieve HTTP 
	request
*/



let http = require('http');


/*
	Using this module's createServer() method, we can create
	an HTTP server that listens to request on a specified port 
	and gives responses back to the client

	the messages sent by the client, usally a web browser, 
	are called "request"

	the  messages sent by the server are answers, called response

	The server will be assigned to port 4000 via "listen(4000)" 
	method where the server will listen to any request that are 
	sent to it

	a port is a virtual point where network connection starts and end
*/
let port = 4000;

http.createServer(function (request, response) {
	/*
		Uses the writeHead() method to:
			
			sets the status code for the response - 200 means OK
			
			sets the content-type of the response as a plain text 
			message

	*/

  //response.writeHead(200, {'Content-Type': 'text/html'});
  response.writeHead(200, {'Content-Type': 'text/plain'});

  response.end('Hello World!');

}).listen(port);

// when server is running, console will print the message
console.log (`Server is live at localhost:${port}`);


/*

$ node index.js
Server is live at at port 4000

*/