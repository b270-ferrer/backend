// S33 Activity:
// 1. In the S33 folder, create an activity folder and an index.html and a script.js file inside of it.
// 2. Link the index.js file to the index.html file.
// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

		fetch('https://jsonplaceholder.typicode.com/todos')
			.then((response) => response.json())
			.then((data) => {
		    	const titles = data.map(item => item.title);
		   	 	console.log(titles);
		  	})
		  	.catch((error) => console.error(error));

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


		itemId = 3; // Replace with the ID of the item you want to retrieve

		fetch(`https://jsonplaceholder.typicode.com/todos/${itemId}`)
			.then((response) => response.json())
			.then((data) => {
				console.log(`The item ${data.title} has the status of ${data.completed}`)
		  	})
			.catch((error) => console.error(error));


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

		data = {
			completed:false,
			id: 201,
			title: "Created To Do List Item",
			userId:1
		};

		fetch("https://jsonplaceholder.typicode.com/todos", {
			method:"POST",
			headers:{
				"Content-Type": "application/json"
			},
			body: JSON.stringify(data)
		})
		.then ((response) => response.json())
		.then ((response) => console.log(response))
		.catch((error) => console.error(error));

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID


		itemId = 1; // Replace with the ID of the item you want to update

		data = {
			title: "Updated To Do List Items",
			description: "To update the my to do list with a different data structure",
			completed: true,
			dateCompleted: "Pending",
			userId: 1
		};

		fetch(`https://jsonplaceholder.typicode.com/todos/${itemId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
		  	},
			body: JSON.stringify(data)
		})
		.then((response) => response.json())
		.then((data) => console.log(data))
		.catch((error) => console.error(error));



// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

		itemId = 1; // Replace with the ID of the item you want to update

		data = {
			status: "Complete",
			dateCompleted: "07/09/21"
		};

		fetch(`https://jsonplaceholder.typicode.com/todos/${itemId}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		})
		.then((response) => response.json())
		.then((data) => console.log(data))
		.catch((error) => console.error(error));

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

		itemId = 1; // Replace with the ID of the item you want to delete

		fetch(`https://jsonplaceholder.typicode.com/todos/${itemId}`, {
			method:"DELETE",
		})
		.then ((response) => response.json())
		.then ((response) => console.log(response))
		.catch((error) => console.error(error));




// 13. Create a request via Postman to retrieve all the to do list items.		  
// - GET HTTP method
// - https://jsonplaceholder.typicode.com/todos URI endpoint
// - Save this request as get all to do list items
// 14. Create a request via Postman to retrieve an individual to do list item.
// - GET HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as get to do list item
// 15. Create a request via Postman to create a to do list item.
// - POST HTTP method
// - https://jsonplaceholder.typicode.com/todos URI endpoint
// - Save this request as create to do list item
// 16. Create a request via Postman to update a to do list item.
// - PUT HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as update to do list item PUT
// - Update the to do list item to mirror the data structure used in the PUT fetch request
// 17. Create a request via Postman to update a to do list item.
// - PATCH HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as create to do list item
// - Update the to do list item to mirror the data structure of the PATCH fetch request
// 18. Create a request via Postman to delete a to do list item.
// - DELETE HTTP method
// - https://jsonplaceholder.typicode.com/todos/1 URI endpoint
// - Save this request as delete to do list item
// 19. Export the Postman collection and save it in the activity folder.
// 20. Create a git repository named S28.
// 21. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 22. Add the link in Boodle.










