//console.log("Hello World!")

//[section] JavaScript Synchronous and Asynchronous
// JavaScript by default is Synchronous, meaning one statement at a time
// JS script will stop at statements with errors 

// console.log("Hello World!")
// //consle.log("Hello Again")
// console.log("Goodbye!")

// Code blocking - waiting for th especific satement to finish before executing the next statement
// for (let i =0; i<=1500; i++){
// 	console.log(`$(i)`)
// }
// console.log("Hello Again")

// Asynchronous means that we can proceed to execute other statement while time-consuming code run in the background


// [section] Getting all post
// Fetch API allows us to asynchronously request for a resource(data)
// It can be used to fetch various types of resurces: text , JSON, HTML, images and more
// fetch() method is JS that is used t request to a server and load the information on the webpage

/*
	syntax:
		fetch("apiUrl")


*/

fetch("https://jsonplaceholder.typicode.com/posts")
 .then (response => console.log(response))


fetch("https://jsonplaceholder.typicode.com/posts")
 .then ((response) => console.log(response.status))

// Returns a "promise"
// a "promise" is an object that represents the eventual completion or failure of a 
// asynchronous function and its resulting value
/*
	A promise may be in one ofthe 3 posible ststes
		1.	pending		-	initial state, neither fulfilled nor rejected
		2.	fulfilled	-	operation was completed
		3.	rejected	-	operation failed
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

fetch("https://jsonplaceholder.typicode.com/posts")
// We use "json()" from the response object to convert the data retrieve to JS objects 
.then ((response) => response.json())
//.then (response => console.log(response))
.then ((response) => {
	//response.forEach(post => console.log(post))
	response.forEach((post) => console.log(post.title))
})

// "async" and "await" keyword are another approach that can be used to achieve asynchronous code

async function fetchData(){
	// waits for the fetch to complete then store the value in the result variable
	let result = await (fetch("https://jsonplaceholder.typicode.com/posts"));
	console.log (result);
	
	//converts the result into JS object
	let json = await result.json();
	console.log(json);
}

fetchData();


// [section] getting a specific post
// ":id" is a wildcard where you can put any value
fetch("https://jsonplaceholder.typicode.com/posts/1")
.then ((response) => response.json())
.then ((response) => console.log(response))



//[section] creating a post
/*
	Syntax:
	fetch("URL", options)
	.then()

*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method:"POST",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello World",
		userId:1
	})
})
.then ((response) => response.json())
.then ((response) => console.log(response))





//[section] updating  a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method:"PUT",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Updated Post",
		body: "Hello Again",
		userId:1
	})
})
.then ((response) => response.json())
.then ((json) => console.log(json))




//[section] updating  a post using the patch method
/*
	PUT vs PATCH
		PATCH is used to update a single/ several properties
		PUT is used to update the whole document

*/

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method:"PATCH",
	headers:{
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post Title"

	})
})
.then ((response) => response.json())
.then ((json) => console.log(json));



//[section] Delete a post 

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method:"DELETE",
})
.then ((response) => response.json())
.then ((response) => console.log(response));