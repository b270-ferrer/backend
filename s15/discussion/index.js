// alert("Hello, Batch 270!");

console.log("Hello again!");

console.log( "Hi,  Batch 270! " );


/*
	- There are two types of comments:
	1. SIngle-Line denoted by two Slashes
	2. Multi-Line denoted by  a slash and asterisk (CTRL + SHIFT + /)

*/


// [Section] Variables
/*
	- It is used to contain data
*/

// Declaring Variables
// Syntax: let/const variableName
/*
	Trying to print out a value of a variable that has 
	not been declared will return an error of "undefined"
	- Variables must be declared first with value before 
	they can be used 
*/

	let myVariable;
	console.log(myVariable);

	let greeting = "Hello"
	console.log(greeting);

// Declaring and initalizing variables
/* Initializing variables = the instance 
	a variable is given its initial value
*/
let productName ='desktop computer'
console.log(productName);
productName ='laptop'
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

// interest = 2.5;
// console.log(interest);


let friend = "kate";
console.log(friend);


// we canot re-declare variables within its scope
friend = "jade";
console.log(friend);


let firstName = "jade";
console.log(firstName);

// [section] Data Type
// Strings- a series of characters that create a word, 
// or a phrase, a sentence or anything related to 
// creating text

let country = "Philippines";
let province = "Batangas";

console.log(country);
console.log(province);

// Concatinating String
// Multiple string values can be combined to create a
// single string using "+" symbol
let fullAddress = province + ", " + country;
console.log(fullAddress);

console.log("I live in the "+ country);

//  "\n" refers to creating  anew line in between text
let mailAddress = "Metro Manila \n\n Philippines"
console.log(mailAddress);

// you can use the escape caharacter \' or ' between " "
let message = 'John\'s employee wnt home early '
console.log(message)

//always us ; as delimiter even if not needed
// Numbers

// Integer /whole number
let headCount = 26;
console.log(headCount);

//decimal number 
let grade = 98.7;
console.log(grade);

// exponencial
let planetDistance = 2e10;
console.log(planetDistance);

// Boolean
let isMarried = false;
let inGoodConduct = true;

console.log(isMarried);
console.log(inGoodConduct);

// Arrays - are special kind of data type that 
// used to store multiple

// Arrays can store differnt data types but 
// is nnormally used to similar data types
// Syntax: let/const arrayName = [elementA, elementB, ...]

let grades =[98.7, 92.1 , 90.2, 94.6];
console.log(grades);

let details =["John", "Smith", 32, true];
console.log(details);

// Objectss
/*
	Syntax:
	let/const objectName ={
		propertyA: valueA,
		propertyB: valueB,
		...
	}


*/

let person = {
	fullName: " Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912345678", "0987654321"],
	address: {
		houseNumber:"345",
		city: "Manila"
	}
};
console.log(person);


let gradesObject = {
	math:98.7, 
	science:92.1 , 
	english:90.2, 
	arts:94.6};
console.log(grades);


// Null

let spouse = null;
console.log(spouse);
spouse = "sunshine";
console.log(spouse);
