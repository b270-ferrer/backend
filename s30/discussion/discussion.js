db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: [ "Philippines", "US"]
	},

	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: [ "Philippines", "Ecuador"]
	},	

	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: [ "US", "China"]
	},	

	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: [ "Philippines", "India"]
	},	
]);


// [ section ] MongoDB Aggregation
/*
	Used to grenerate manipulated data and perform operations to create 
	filtered results tht help in analyzing data
*/

// [sub-section] Using the agregation method
/*
	the "$match" is used to pass the documents that meet the specified 
	condition(s) to the next pipeline stage/aggregation process

	syntax: 
		{$match: {field: value}}


	syntax:
		{$group: {_id:"value", fieldResult:"valueResult"}}

		using the "$match" and "$group" along with aggregation will 
		find for products that are on sale and will group the total 
		amount of stock for suppliers found

	syntax: 
		db.collectionName.aggregate ([
			{ 	$match: {fieldA:valueA}	},
			{	$group: { _id: fieldB}, {result: {operations}
			}}
		])

	The symbol "$" will refer to a field name that is available in 
	the document that are being aggregaate on.

*/

db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $group: {_id: "$supplier_id", total123456: {$sum:"$stock"}}}
	//will look at the differemt distinct "$supplier_id" key-value pair 
	])

db.fruits.aggregate(
	[
		{ 
			$match: {onSale: true} 
		},
		{ 
			$group: {
				_id: "$supplier_id", total: {$sum:"$stock"}
			}
		}
	]
)


db.fruits.aggregate([
	{ $match: {onSale: true} },
	{ $group: {_id: "supplier_id", total: {$sum:"$stock"}}} 
	//will look only at the "supplier_id" key and ignore the different key-calue pair
])



// [sub-section] field projection with aggregation

/*
	the $project can be used when aggregating data to include/exclude fields from the returned result

	syntax:
		{$project: {field:1/0}}
	
*/

db.fruits.aggregate([
	{ 	$match: 	{onSale: true} 									},
	{ 	$group: 	{_id: "$supplier_id", total: {$sum:"$stock"}}	},
	{	$project: 	{_id: 0}										}	
])


// [sub-section] sorting aggregated result

/*
	the $sort can be used to change the order of aggregated results 

	providing a value -1 qill sort the aggregated results in reverse order

	syntax:
		{$sort: {_id: 	1/-1}}


	
*/

db.fruits.aggregate([
	{ 	$match: 	{onSale: true} 									},
	{ 	$group: 	{_id: "$supplier_id", total: {$sum:"$stock"}}	},
	{	$sort: 		{total: -1}										}	
])

// [sub-section] aggregating results based on array fields

/*
	The "$unwind" deconstruct an array field from a collection/field
	with an array value to output a result for each element

	syntax:
		{$unwind: "field"}

	
*/

db.fruits.aggregate([
	{ 	$unwind: 	"$origin" 			}
])


// [sub-section] display fruits documents by thier origin and the kind of fruts that are supplied



db.fruits.aggregate([
	{ 	$unwind: 	"$origin" 							},
	{ 	$group: 	{_id: "$origin", kinds: {$sum:1}}	}
])













