// discussion.js
// CRUD Operations
/*
	-CRUD operatons are the heart of any backend operations
	- Mastering the CRUD opertions of any kanguage makes us a valuable developer
	and makes the work esier for us to del with huge amount of information

*/

// [section] inserting documents (create)
/*

	Syntax:
		db.collections.insertOne({object})

		Insert Many:
		db.collections.insertMany([{objectA}, {objectB}])

*/


db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact : {
		phone : "09123456789",
		email: "janedoe@email.com"
	},
	courses: ["CSS", "JavaScript","Python"],
	department: "none"
});


// Insert MAny (paste then ctrl enter)

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact : {
			phone : "09123451234",
			email: "stephenhawking@email.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	}, 
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact : {
			phone : "0956785678",
			email: "neilarmstrong@email.com"
		},
		courses: ["React", "Laravel","SASS"],
		department: "none"
	}, 
]);

// [section] finding documents (read/retrieve)
/*
		-syntax:
			db.collectionName.find();
			db.collectionName.find({field : value});

*/
// retrieve all
	db.users.find();


// find a single document
	db.users.find({firstName : "Stephen"});
	db.users.find({age: 82});

// find document with multiple parameter
/*
		-syntax:
			db.collectionName.find({fieldA : valueA,  fieldB : valueB});

*/	
		db.users.find(	{	lastName : "Armstrong", age:82	}	);
		db.users.find(	{	firstName : "JAne", age:21		}	);


//[section] updating documents (update)

db.users.insertOne({
	firstName: "Test",
	lastName: "Test",
	age: 0,
	contact : {
		phone : "000000000",
		email: "test@email.com"
	},
	courses: [],
	department: "none"
});


// updating a single document
/*
	-just like the "find" method, method will modify only the first instance
	that matches the criteria 

	syntax:
	db.collectionName.updateOne({criteria}, {$set:{field:value}})
	

*/

db.users.updateOne(
	{firstName: "Test"}, 
	{
		$set:{
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact : {
				phone : "123456789",
				email: "billgates@email.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
);


// updating a multiple document
/*
	-just like the "find" method, method will modify all  instances
	that matches the criteria 

	syntax:
	db.collectionName.updateMany({criteria}, {$set:{field:value}})
	
	to run: paste to robo3t console, ctrl + enter
*/

db.users.updateMany(
	{department:"none"},
	{
		$set: {
			department: "HR"
		}
	}

);

//Replace One
/*
	- can be used to replace the whole document if nescessary
*/

db.users.replaceOne(
	{
		firstName:"Bill"
	},
	{
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
	}
);


db.users.updateOne(
	{
		firstName: Test
	},
	{
		$set:{
			{firstName: "Charles"}
		}

	}

);

db.users.replaceOne(
    {
    	firstName:"Charles"
    },
    {
       
        lastName:"Ferrer",
        age: 10,
        department: "Operations"
        
    }
);

// Deleting documents (delete)

// document to delete
db.users.insertOne(
	{
		firstName: "Test"
	}
)


// Deleting a single Documwnt
/*
	- Syntax:
		db.collectionName.deleteOne(criteria)
*/

db.users.deleteOne(
	{
		firstName: "Test"
	}
);


// Deleting a multiple Document
/*
	- Be careful when using the deleteMany. If no search criteria is provided, it will delete all in the database
	- Syntax:
		db.collectionName.deleteMany(criteria)
*/

db.users.deleteMqny(
	{
		firstName: "Bill"
	}
);

// will not delete anything
db.users.deleteMany(
	{
		firstName: "Bill",
		lastName:"Bob"
	}
);

//[section] advance queries

//querying an embeded document
db.users.find({
	contact: {
		phone : "09123451234",
		email: "stephenhawking@email.com"
	}

});

db.users.find({
	"contact.email": "janedoe@email.com"
});

//querying an array with regard to order
// order of array IS considered
db.users.find({
	courses: ["CSS", "JavaScript","Python"]
});
//no output
db.users.find({
	courses: ["JavaScript", "CSS", "Python"]
});



// querying an array without regard to order
db.users.find({
	courses: {$all: ["React", "Python"]}
});
//same output
db.users.find({
	courses: {$all: ["Python", "React"]}
});


//Querying an embedded array
db.users.insertOne(
	{

		namearr:[
			{namea: "juan"},{nameb:"tamad"}
		]
	}
)


db.users.find({
	namearr:{namea: "juan"}
})













