 console.log("hello");



// Array Methods

 // Javascript has build in functions and methods for array. This Allow us to
 // manipilate and access arrat items


 // [Section] Mutator Methods
 // 	- these are methods that "mutate" or change an array after they are 
 // 	created
 // 	- These metthods manipulate the original array performing various task
 // 	such as adding and removing elements
 // 


let fruits = ["Apple","Orange","Kiwi","Dragon Fruit"];

// push()
// 
//  -Adds and element at the end of an array and return the new array length
// 
// 	-Syntx:
//		arrayName(elements....) 
// 


console.log(" Current fruit array:");
console.log(fruits);

let fruitLength = fruits.push("Mango");
console.log(fruitLength);
console.log(" Mutated array from push method:");
console.log(fruits);


// adding multiple elemtny to an array 
fruits.push("Avocado", "Guava");
console.log(" Mutated array from push method:");
console.log(fruits);


// pop()
// removes the last element in an array

let removeFruit = fruits.pop();
console.log(removeFruit); //Guava
console.log(" Mutated array from pop method:");
console.log(fruits);



// unshift()
// adds one or more elements at the beginning of an array and returns the new
// array
// Syntax:
// 		arrayName.unshift(elementA, elementB,.....)

fruits.unshift("Lime","Banana");
console.log(" Mutated array from unshift method:");
console.log(fruits);

// shift()
// removes an elemnt at the beginning of the array and returns that element
// array
// Syntax:
// 		arrayName.unshift(elementA, elementB,.....)


let anotherFruit = fruits.shift();
console.log(anotherFruit); //lime
console.log(" Mutated array from shift method:");
console.log(fruits);


// splice()
// 
//	-simultaneously removes elements from a specific index number and adds element
//	- returns removed elements; 
// 	-syntax:
//		arrayName.splice(startingIndex,deleteCount, elementsToBeAdded ) 


let fruitSplice = fruits.splice(1,2, "Lime", "Cherry")
console.log(fruitSplice); //lime
console.log(" Mutated array from splice method:");
console.log(fruits);

// reverse()
// 
// -Reverses the order of the array elements 
//	- Syntax: 
//		arrayName.reverse()


fruits.reverse();
console.log(" Mutated array from reverse method:");
console.log(fruits);

// sort()
 
// -Rearranges the array elements in alphabetical order
// 	- Syntax: 
// 		arrayName.sort()


 fruits.sort();
 console.log(" Mutated array from sort method:");
 console.log(fruits);

// NON Mutator Methods
//  
// 
// -Non mutator methods are methods that do not modify or change an array 
// after thry are created
// -thse methods do not manipualte the original array performing various task
// such as returning elements from an array or combining array and printing 
// outputs


let countries = ["US","PH","CAN","SG","TH","PH","FR","DE"];
console.log(countries);


// indexOf()
// 
//		-returns the index number of the first maching elemnt found in
//	in the array 
// 		-returns -1 if no match found
// 		- the search proess wiil start from the first element onward
// 		-syntax
//				arrayName.indexOf(searchValue)
// 				arrayName.inndexOf(searchValue, startingIndex)

let firstIndex = countries.indexOf("PH", 2);
console.log("Result of IndexOf method: " + firstIndex);


let invalidCountry = countries.indexOf("BR");
console.log("Result of IndexOf method: " + invalidCountry);


// lastIndexOf()
// 	- Returnsindex number of the last matching element found in the array 
// 		-returns -1 if no match found
// 		- the search proess wiil start from the last element backward
// 		-syntax
//				arrayName.lastIndexOf(searchValue)
//				arrayName.lastIndexOf(searchValue, lastIndex)

let lastIndex = countries.lastIndexOf("PH",4);
console.log("Result of lastIndexOf  method: " + lastIndex );


// slice()
// 	- portion/slices of element from an array and returns a new array
// 	-Syntax:
// 		arrayname.slice (startingIndex);
// 		arrayname.slice (startingIndex);
// 

let sliceArrayA = countries.slice(2);
console.log("Result of slice  method: " + sliceArrayA );
//[CAN,SG,TH,PH,FR,DE]

let sliceArrayB = countries.slice(2,5);
console.log("Result of slice  method: " + sliceArrayB );
//[CAN,SG,TH]

let sliceArrayC = countries.slice(-3);
console.log("Result of slice  method: " + sliceArrayC );
//[PH,FR,DE]

// toString()
// 
// 		-Returns an array as a string seperated by commas
// 

let stringArray = countries.toString();
console.log("Result of toString  method: ")
console.log(stringArray);

// concat()
// 
// 		Combines array/s and returns the combined result/array
// 		Syntax
// 			arrayName.concat (arrayB)
// 
// 

let taskArrayA = ["drink html","eat javascript"];
let taskArrayB = ["inhale css","breath bootstrap"];
let taskArrayC = ["get git","be node"];

let tasks = taskArrayA.concat(taskArrayB)
console.log("Result of concat  method: ")
console.log(tasks);
//['drink html', 'eat javascript', 'inhale css', 'breath bootstrap']

// Combines multiple arrays
let allTasks = taskArrayA.concat(taskArrayB,taskArrayC)
console.log("Result of concat  method: ")
console.log(allTasks);
//['drink html', 'eat javascript', 'inhale css', 'breath bootstrap', 'get git', 'be node']

// Combining array with elements
let combinedTasks = taskArrayA.concat("smell express","throw react")
console.log("Result of concat  method: ")
console.log(combinedTasks);
//['drink html', 'eat javascript', 'smell express', 'throw react']


// join()
// 	-returns a string containing the array elements
// 
// 


let users = ["john","jane","joe","joshua"]
console.log(users.join());
console.log(users.join("\n"));


// [Section] Iteration Method 
// forEach()
// 		-iterates to each element of the array and run each item inside the anonymous
//  function
//  	- No Return Value
// 
//	Syntax:
// 		arrayName.forEach (
// 			function (individualElement){
// 				statements
// 			}
// 		)
// 
// 
// 
// 

let filteredTasks = [];
allTasks.forEach(
	function(task){
		if(task.length> 10){
			filteredTasks.push(task);
		}
	}
)
console.log("Result of filtered Task: ")
console.log(filteredTasks);


// map()
// 		-iterates to each element of the array and run each item inside the anonymous
//  function
//  	- Returns a new array
// 
//	Syntax:
// 		arrayName.map (
// 			function (individualElement){
// 				statements
// 				return
// 			}
// 		)
// 
// 
// 
// 
let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(
					function(number){
						return number * number
					}
				)

console.log("Original Array: ")
console.log(numbers);
console.log("Result of Map method: ")
console.log(numberMap);


// map() vs forEach()
console.log(numbers);//[1, 2, 3, 4, 5]

let numberForEach = numbers.forEach(
						function (number){
							return (number * number);	
						}
					);


console.log(numberForEach); //undefined - because forEach() does not return anything

// every()
// 
// 		-Check if all elements in an array meetsthe given condition
// 		-Returns a true value if all element meet the condition and flase if 
// 		otherwise 
// 
// 
// 


let allValid = numbers.every (
					function(number){
						return (number < 3);
					}
				);

console.log("Result of every method: ")
console.log(allValid);//false

// some()
// 
// 		-Check if at least one element in an array meetsthe given condition
// 		-Returns a true value if all element meet the condition and flase if 
// 		otherwise 
// 
// 
// 


let someValid = numbers.some (
					function(number){
						return (number >2);
					}
				);

console.log("Result of some method: ")
console.log(someValid);


//filter
// 
// 		- Returns a new array that contains the element which meet the 
// 		given condition
// 		- Returns am empty array if no element were found
// 
let filterValid = numbers.filter (
					function(number){
						return (number <3);
					}
				);

console.log("Result of filter method: ")
console.log(filterValid);



// includes ()
// 
//	-check if the argument passed is inside array 
//  - return true if found
// 


let products = ["Mouse","Keyboard","Laptop","Monitor"]

let productFound1 = products.includes("Mouse")
console.log(productFound1);


let productFound2 = products.includes("Headset")
console.log(productFound2);



// reduce()
// 
//		- Evaluates element from left to right and retrun/reduce 
// 		the array into a single value
// 		-note: best think of it as summation E
// 

let iteration =0;
console.log(numbers);
let reducedArray = numbers.reduce(
						function(x,y){
							console.warn ("Current itration: "+ ++iteration);
							console.log ("accumulator: "+ x);
							console.log ("Current Value: "+ y);
							return x + y;			
						}

					)
console.log("Result of reduce method: " + reducedArray);

// 1. x = Hello, y =Again 			=> Hello Again
// 2. x = Hello Again, y = World 	=> Heloo Again World

let list = ["Hello", "Again", "World"];

let reducedString = list.reduce(
							function (x,y){
								return x + " " + y;
							}
						);

console.log("Result of reduce method: " + reducedString);









