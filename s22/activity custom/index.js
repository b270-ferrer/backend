// JAVASCRIPT ARRAY MANIPULATION
// Activity 1
// Write a function to turn all elements of the following array into string.
// socialMedia = ["Facebook", "Twitter", "Instagram","Tiktok"]

let socialMedia = ["Facebook", "Twitter", "Instagram","Tiktok"]

function arrayToString(inputArray){
    return inputArray.join("");
}
console.log ("ACTIVITY 1");
console.log (arrayToString(socialMedia));

// Activity 2
// Create a function that will add two Avengers into the the following array. 
// 1 at the beginning and 1 in between the other element.
// avengers = ["Ironman", "Captain America", "Thor"]

let avengers = ["Ironman", "Captain America", "Thor"]

function addAvenger(avenger1,avenger2){    
    avengers.unshift(avenger1);
    avengers.push(avenger2);
}

console.log ("ACTIVITY 2");
console.log ("array before function:  "+avengers);
addAvenger("Hulk","Antman")
console.log ("array after function:   "+avengers);

// Activity 3
// Create an array of your friends' name. Create a function that will remove 
// the last name in the array. Then, replace the deleted name with other two names.

friends = ["Rachel", "Monica", "Phoebe", "Joey", "Chandler", "Ross"];

function replaceFriends(friend1,friend2){
    friends.pop();
    friends.push(friend1,friend2);
}

console.log ("ACTIVITY 3");
console.log ("array before function:  "+friends);
replaceFriends("Izuku Midoriya","All MIGHT")
console.log ("array after function:   "+friends);
