let http = require('http');
const port = 4000;

const server = http.createServer((request, response) => {
	/*	HTTP Method of incomming request can be accessed 
	via "method" property in request paramater

		GET means that we will be retrieving/reading information

	*/

	if (request.url == `/items` && request.method == `GET`){
		response.writeHead(200, {'Content-Type': 'text/plain'});	//200 means "OK" or success
		//Ends the response cycle
		response.end('Data retrieved from the database');
	}
	if (request.url == `/items` && request.method == `POST`){
		response.writeHead(200, {'Content-Type': 'text/plain'});	//200 means "OK" or success
		//Ends the response cycle
		response.end('Data to be send to the database');
	}

})
server.listen(port);

console.log (`Server is live at localhost:${port}`);
