let http = require('http');
const port = 4000;

const server = http.createServer((request, response) => {
	//homepage GET route - http://localhost:4000/
	if (request.url == `/` 				&& 		request.method == `GET`){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the booking system');
	} else 

	//Course POST route - http://localhost:4000/addCourse
	if (request.url == `/addCourse` 		&& 		request.method == `POST`){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add course to our resources');
	} else 

	//Course PUT route - http://localhost:4000/updateCourse
	if (request.url == `/updateCourse` 	&& 		request.method == `PUT`){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources');
	} else 

	//Course DELETE route - http://localhost:4000/archiveCourse
	if (request.url == `/archiveCourse` 	&& 		request.method == `DELETE`){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive course to our resources');
	} else 

	//Course GET route - http://localhost:4000/courses
	if (request.url == `/courses` 		&& 		request.method == `GET`){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`Here's our course available`);
	} else 

	//Profile GET route - http://localhost:4000/profile
	if (request.url == `/profile` 		&& 		request.method == `GET`){
		
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile');
	} else {

	// all other routes will return a "404 page not available"
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('404 page not available');
	}
	
	
})
server.listen(port);

console.log (`Server is live at localhost:${port}`);
