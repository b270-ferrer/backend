
//[section] OBJECTS
// 
// - An object is a data type that is used to represent real
// world objects
// - information is stored and represented in a "key-value" pair
// - Syntax:
//    let objectName = {
//      keyA: valueA,
//      keyB: valueB
// }


// Creating object using initializer/literal notation

let celphone = {
    name: "Nokia 3210",
    manufactureDate: 1999
};



console.log ("Result from creating objects using initializers/literal notation:");
console.log (celphone);
console.log (typeof celphone);


// Creating objects using a constructor function
// 
//  - Creates a reusable function to create several objects that 
// have the same data structure
// 
//  - an "instance" is a concrete occurance of any object which 
//  empesizes on distinct/ unique
// 
//  -Syntax:
//     function ObjectName(keyA,KeyB){
//         this.keyA = keyA,
//         this.keyB = keyb
//     }



// The "this" keyword allow us to assign a new object's property by
// associating them with the value recievedfrom a constructor function's
//  parameter


function Laptop (name, manufactureDate){
    this.name = name,
    this.manufactureDate = manufactureDate;
}

// This is a unique instance of the Laptop object
let laptop = new Laptop ("Lenovo", 2008);
console.log("Result from creating using object constructors:");
console.log (laptop);
console.log (typeof laptop);


// This is another unique instance of the Laptop object
let myLaptop = new Laptop ("MacBook Air", 2020);
console.log("Result from creating using object constructors:");
console.log (myLaptop);
console.log (typeof myLaptop);

// this only calls the Laptop function instead of 
// creating a new object instance
let oldLaptop = Laptop ("Acer", 1980);
console.log (oldLaptop);            //undefined
console.log (typeof oldLaptop);     //undefined


//Creating empty objects
let computer = {};
console.log (computer);

let myComputer = new Object();
console.log (myComputer);


//[Section] Accesing Object properties

// Using the dot notation
console.log("Result from dot notation: " + myLaptop.name); //Macbook Air

// Using the square bracket notation
console.log("Result from dot notation: " + myLaptop["name"]); //Macbook Air


// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
let car ={}


// Initializing/Adding Object Properties
car.name = "Honda Civic"
console.log("Result from adding properties using dot notation")
console.log(car)

car["manufactureDate"] = "2008"
console.log("Result from adding properties using [] notation")
console.log(car)

// Reassigning object properties
car.name = "Ford Raptor"
console.log(car)


// Deleting object properties
delete car["manufactureDate"];
console.log("Ressult from deleting properties: ");
console.log(car);

// [section]Object Methods
// 
//  -   a method is  function property of an object
//  -   Methods are useful for creating object specific 
//      function which are used to perform task on them

let person = {
    name: "John",
    talk: function(){
        console.log ("Hello! My name is " + this.name);
    }
}
console.log(person);
console.log("Result from object method: ");
person.talk();

person.talk; //no output


// Adding methods to objects
person.walk = function(){
    console.log(this.name + " walked 25 steps forward");
}

person.walk()
console.log(person);


let friend = {
    firstName: "Joe",
    lastName: "Smith",
    address:{
        city: "Austin",
        county: "Texas"
    },
    email: ["joemail.com","joesmith@email.com"],
    introduce: function(){
        console.log("Hello, My name is "+ this.firstName + " "+this.lastName)
    }

}


friend.introduce(); // Hello!, my name is Joe Smith



/*
    Senario 
    1. We would like to create a game that would have seral pokemon 
    interact with each other
    2. Every pokemon would have the same set stat, propwerties, and functions

*/


let myPokemon = {
    name:" Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This pokemon tackeled target Pokemon")
        console.log("target Pokemon's health is now  reduced to target Pokemon health")
    },
    faint: function(){
        console.log("pokemon fainted")
    }

}
console.log (myPokemon)


function Pokemon (name, level){
    //Properties
    this.name = name,
    this.level = level,
    this.health = 2*level,
    this.attack = level,
    this.tackle = function(target){
       console.log(this.name + " tackled " + target.name);
       console.log("target Pokemon's health is now  reduced to target Pokemon health")
    },
    this.faint = function(){
        console.log(this.name + " fainted")   
    }
}


// Creates new instances of "Pokemon" objects each with their unique properties
let pikachu = new Pokemon ("Pikachu", 16);
console.log(pikachu)


let rattata = new Pokemon ("Rattata", 8);
console.log(rattata)


// Provides the "rattata" object as an argument to the "pikachu" tackle method
// creates an interaction between pokemon/ objects
pikachu.tackle (rattata)

rattata.tackle (pikachu)






















