// 1. In the S23 folder, create an activity folder and an index.html and script.js file inside of it.
// 2. Link the script.js file to the index.html file.
// 3. Create a trainer object using object literals.
// 4. Initialize/add the following trainer object properties:
// - Name (String)
// - Age (Number)
// - Pokemon (Array)
// - Friends (Object with Array values for properties)


let trainer ={
    name: "Ash Ketchum",
    age: 14,
    pokemon:["Pikachu",
        "Butterfree",
        "Pidgeot",
        "Bulbasaur",
        "Charizard",
        "Squirtle",
        "Kingler",
        "Primeape"],
    friends: {
        kanto: ["Misty","Brock"],
        hoenn: ["May","Max"],
        sinnoh: ["Dawn"],
        unovah:["Iris","Cilan"]        
    }
};


// 5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!


trainer.talk = function(){
    console.log("Pikachu! I choose you!");
}
console.log(trainer);


// 6. Access the trainer object properties using dot and square bracket notation.


console.log("Result of Dot Notation");
console.log(trainer.name)    
console.log("Result of Square Bracket Notation");
console.log(trainer["pokemon"])


// 7. Invoke/call the trainer talk object method.


console.log("Result of talk method");
trainer.talk();


// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the constructor)
// - Level (Provided as an argument to the constructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)


function Pokemon(name, level){
    this.name   = name,    //(Provided as an argument to the constructor)
    this.level  = level,   //(Provided as an argument to the constructor)
    this.health = 2*level, //(Create an equation that uses the level property)
    this.attack = level    //(Create an equation that uses the level property) 
    this.tackle = function(target){
        // 10. Create a tackle method that will subtract the health property of the target 
        // pokemon object with the attack property of the object that used the tackle method.
        target.health = target.health - this.attack
        console.log(this.name + " tackled " +target.name)
        console.log(target.name +"'s Health is now reduced to "+target.health);
        // 12. Create a condition in the tackle method that if the health property of the target 
        // pokemon object is less than or equal to 0 will invoke the faint method.
        if (target.health<=0){
            this.faint(target);
        }
    }
    this.faint = function(target){
        // 11. Create a faint method that will print out a message of targetPokemon has fainted.
        console.log(target.name +" has Fainted");
    }
    

}


// 9. Create/instantiate several pokemon object from the constructor with varying name and level properties.


let pikachu = new Pokemon ("Pikachu",12);
console.log(pikachu);
let geodude = new Pokemon ("Geodude",8);
console.log(geodude);
let mewtwo  = new Pokemon ("Mewtwo",100);
console.log(mewtwo);


// 13. Invoke the tackle method of one pokemon object to see if it works as intended.

mewtwo.tackle (geodude)
console.log(geodude);


// 14. Create a git repository named S23.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.






















