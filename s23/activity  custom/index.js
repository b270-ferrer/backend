// JAVASCRIPT OBJECTS
// Activity 1
// Create an object variable containing different properties of a 
// product a product being sold. Write a function that returns a 
// short description of the product using all the its declared properties.
console.log ("ACTIVITY 1");

let product = {
    name :           "6 - pc. Chickenjoy Solo"
    ,price :         435.00
    ,quantity :      999
    ,tagline :      "A bucket of your favorite crispylicious, juicylicious Chickenjoy!"
};


function description(obj){
    output ="";
    for (let property in obj)    
        output +="\n" + property + ": " + obj[property];    
    return output;  
}
console.log ( description(product) );

// Activity 2
// Write a function that can add new propeties to the created object 
// in Activity 1 by assigning a value to a new property using dot or 
// bracket notation. Display the object in the console.

console.log ("ACTIVITY 2");

function addProperty(obj, name, value){
    obj[name] = value;
}
addProperty(product, "sale", true);
console.log ( description(product) );

// Activity 3
// Create a "person" object with the following properties:
//     1. firstName:   String
//     2. lastName:    String
//     3. age:     Number
//     4. talk:        function/method
// Using the "talk" method, let the "person" talk by printing the 
// messafe "Hi, I am (full name) and I am (age) years old".

console.log ("ACTIVITY 3");

let person = {
    firstName:   "Monkey",
    lastName:    "Luffy",
    age:        19,
    talk:       function(){
                    console.log("Hi, I am " + this.firstName + " " + this.lastName + " and I am " + this.age + " years old");
                }
};

person.talk()