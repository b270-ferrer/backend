//console.log("Hello World!")
//node package manager npm
// type
//	 npm init 
//   npm install express
// create 
//	.gitignore
// inside file
//		node_modules




const express = require("express");
// This creates an express application and stores his in a constant called app
// this is our server
const app = express();

const port = 3000;

// middleware
// 	- is a request handler that has acess to the application's resquest-response cycle
app.use (express.json());

//allows our app to read data from forms
app.use(express.urlencoded({extended:true}));





// [section] routes
// Express has methods corresponding to each Http method

// This route expects to recieve the GET request at the base URI "/"
// This rouse will return a simple message back to the client
app.get("/", (req,res)=>{
	// res.send uses the Express JS module's method to send a response back to the client
	res.send("Hello World")
});


// This route expects to recieve the POST request at the base URI "/hello"
// Body - Raw - JSON
app.post("/hello", (req,res)=>{
	// res.send uses the Express JS module's method to send a response back to the client
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
});


// mock database

let users = [];
app.post("/signup", (req, res) => {
	console.log(req.body)

	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} is successfully registered!`)
	} else {
		res.send(`Please input BOTH username and password.`)
	}
})




// this route expects to recieve a PUT request at the URI "/change-passworf"
app.put ("/change-password", (req,res) =>{
	let message;

	// creates a for loop that will loop through the elements of the "users" arrray

	for (i=0; i<users.length;i++){
		// Changes the password of the user found by the loop into the password provided in the client
		if (req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated`;
			break;
		// if no user was found
		} else {
			message = `user does not exist`
		}
	}

	res.send (message);
})


//////////////////////// ACTIVITY //////////////////////////////
// S34 Activity:
// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.
// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.
// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.
// 7. Export the Postman collection and save it inside the root folder of our application.
// 8. Create a git repository named S34.
// 9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 10. Add the link in Boodle.



//activity
//1. "/home" route
app.get("/home", (request,response)=>{
	response.send("Welcome to the homepage")
});


//2. "/users" route
app.get('/users', (request,response) => {
  response.send(users);
});


//3. "/delete-user"
app.delete('/delete-user', (request,response) => {
	if ( users.some(user => user.username === request.body.username) ){	
		users = users.filter(user => user.username !== request.body.username);
		message = `User ${request.body.username} has been deleted`;
	} else {
 		message = `User ${request.body.username} has not been found`;
 	}
  	response.send(message);
});




app.listen( port, () => console.log(`Server is active at port: ${port}`));