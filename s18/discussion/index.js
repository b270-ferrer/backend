// console.log("hello");


// Function Parameters and Argument

	// function printInput(){
	// 	let nickname = prompt("Enter your nickname");
	// 	console.log("hi, " + nickname);
	// }

	// printInput();


// Consider this function

	function printName(name){
		console.log("My name is " + name);
	}
	printName("Juana");
	printName("John");



	// "name" - parameter
	// a parameter acts as a container/named varible that exist only inside function

	// "Juana" - argument
	// An argumant is the information provided directly into the function

	// Variables can also be passed as an argument
	let sampleVariable = "yui";
	printName(sampleVariable)


	// 
	function checkDivisibilityBy8(num){
		let remainder = num%8;
		console.log ("The remainder of " +num +" divided by 8 is : "+remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);


// function as Arguments

	function argumentFunction(){
		console.log(" This function was passed as an argument before the message was printed")
	}

	function invokeFunction(argumentFunctionNewName){
		argumentFunctionNewName();
	}

	invokeFunction(argumentFunction);

// Multiple Parameters
	function createFullName(firstName, middleName, lastName){
		console.log (firstName + " " + middleName + " " + lastName)
	}

	createFullName("Juan", "Dela", "Cruz");
	
	//providing more/less parameter will not throw an error 
	createFullName("Juan", "Dela");// lastName will be undefined


	fName ="John";
	mName="Doe";
	lName="Smith";
	createFullName(fName,mName,lName);


	createFullName(fName,mName,lName);

// Return 
	function returnFullName(firstName, middleName, lastName){
		console.log("this message will  be printed")
		return firstName + " " + middleName + " " + lastName
		console.log("this message will not be printed")
	}


	let completeName = returnFullName("Jeffrey", "Smith","Bezos")

	console.log(completeName);


	function printPlayerInfo(username, level, job){
		console.log("Username 	: " +username);
		console.log("Level 		: " +level);
		console.log("Job		: " +job);
	}


	let user1 = printPlayerInfo("white_knight", 95, "Paladin");
	console.log(user1);


	function returnAddress(city, country){
		let fullAddress = city + " , " + country;
		return fullAddress
	}

	let myAddress =  returnAddress("Cebu City", "Philippines");
	console.log(myAddress);
// 

// 
	function printSum(num1, num2){
		return num1 + num2;

	}

	function getProduct(num){
		let product = num * 5;
		console.log (product);

	}

	getProduct(printSum(5,5));






























