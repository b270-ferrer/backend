 console.log("hello");

// {SECTION] Loops

// [WHILE LOOP]
 /*
	Syntax:
	while (expresion/condition){
		statement
	}


 */ 

let count = 5;
while (count !== 0 ){
	console.log("While: "+ count)
	count--;
	// "Iteration" - repetition of statement
	// infinte loop crash browser!!!!!!!!
}


// [DO-WHILE LOOP]
// does the statement block at least once before testing
 /*
	Syntax:
	 do {
		statement
	}while (expresion/condition)


 */ 

let number = Number(prompt("Give me a number"))


do {
	console.log("Do While: "+ number)
	number+=1;

}while (number < 10 )


// [FOR LOOP]
// does the statement block at least once before testing
 /*
	Syntax:
	 for (initialization; expression; finalExpression) {
		statement
	}


 */ 


for (let count=0; count<=20;count++){
	console.log("FOR LOOP: "+ count)
	
}

let sampleString="hello world";
// console.log("0 "+ sampleString[0]);
// console.log("1"+ sampleString[1]);
// console.log("2 "+ sampleString[2]);
// console.log("3 "+ sampleString[3]);
// console.log("4 "+ sampleString[4]);
// console.log("5 "+ sampleString[5]);
// console.log("6 "+ sampleString[6]);
// console.log("7 "+ sampleString[7]);
// console.log("8 "+ sampleString[8]);
// console.log("9 " + sampleString[9]);
// console.log("10 " + sampleString[10]);

//console.log("11 " + sampleString[11]);

for (let index=0; index<sampleString.length; index++){
	console.log(index +" = " +sampleString[index])
}
if (sampleString[11] == undefined) {
	console.log("11 error")
}


// for (let count2=50; count2>=0;count2--){
// 	console.log("FOR : "+ count2)
	
// }


let myString = "alex"
// String length  = "string".length
console.log (myString.length)


let myName="AlEx";
for (let i =0; i <myName.length; i++){
	if ( 
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){
			console.log(3);
	}else{
			console.log(myName[i])
	}
	
}

// CONTINUE AND BREAK
console.log("CONTINUE AND BREAK START");
for (let count=0; count<20; count++){
	if (count % 2 === 0){
		// Tells the code to contnue to the next iteration of the loop
		// it ignores all staments loacted after the continue
		continue;
	}
	// the current valueof count is printed out if the remainer is not equal to 0
	console.log("CONTINUE AND BREAK: "+ count);

	if (count>10){
		// terminates the loop once count > 10
		break;
	}
}



let name = "alexandro"

for (let i=0; i<name.length; i++){
	console.log(name[i])
	if (name[i].toLowerCase() === 'a'){
		console.log("continue to the next iteration");
		continue;
	}
;

	if (name[i]==='d'){
		console.log("CONTINUE AND BREAK: "+ i);
		break;
	}
}




































