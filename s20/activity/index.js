/*
   1. In the S20 folder, create an activity folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable number that will store the value of the number provided by the user via prompt.
4. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
5. Create a condition that if the current value is less than or equal to 50, stop the loop.
6. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
7. Create another condition that if the current value is divisible by 5, print the number.
8. Create a variable that will contain the string supercalifragilisticexpialidocious.
9. Create another variable that will store the consonants from the string.
10. Create another for loop that will iterate through the individual letters of the string based on its length.
11. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
12. Create an else statement that will add the letter to the second variable.
13. Create a git repository named S20.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.
*/

// 3. Create a variable number that will store the value of the number provided by the user via prompt.
    let number = Number(prompt("Give me a number"));
    console.log ("The number you provided is " + number);

// 4. Create a for loop that will be initialized with the number provided by the user,
// will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
    for (let index = number; index>0; index--){

// 5. Create a condition that if the current value is less than or equal to 50, stop the loop.
        if (index <= 50){
            console.log ("the current value is at " + index +". Terminating the loop");
            break;
        }

// 6. Create another condition that if the current value is divisible by 10, 
// print a message that the number is being skipped and continue to the next iteration of the loop.
        if (index % 10 === 0){
            console.log ("The number is divisible by 10; Skipping the number");
            continue;
        }

// 7. Create another condition that if the current value is divisible by 5, print the number.
        if (index % 5 === 0){
            console.log (index);
            continue;
        }
    }

// 8. Create a variable that will contain the string supercalifragilisticexpialidocious.
    let inputString = "supercalifragilisticexpialidocious";
    console.log (" Input String is " + inputString);

//9. Create another variable that will store the consonants from the string.
    let vowelLessString ="";

// 10. Create another for loop that will iterate through the individual letters of the string based on its length.
for (let stringIndex = 0; stringIndex < inputString.length; stringIndex++){

// 11. Create an if statement that will check if the letter of the string is equal to a vowel 
//  and continue to the next iteration of the loop if it is true.
    if (        inputString[stringIndex] === 'a' ||
                inputString[stringIndex] === 'e' ||
                inputString[stringIndex] === 'i' ||
                inputString[stringIndex] === 'o' ||
                inputString[stringIndex] === 'u'        ){
        continue;

// 12. Create an else statement that will add the letter to the second variable.
    } else {
        vowelLessString += inputString[stringIndex] ;
    }
}   
console.log (" Vowel-Less String is " + vowelLessString);





