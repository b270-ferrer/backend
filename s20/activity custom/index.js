


// Activity 1
// Using a for loop, change every second letter of a string to an uppercase 'O

function changeSecondLetterToO(input) {
  let output = '';
  for (let i = 0; i < input.length; i++) {
    if (i % 2 === 1) { // check if the index is odd
      output += 'O';
    } else {
      output += input[i];
    }
  }
  return output;
}

let  activity1String = "ABCDEFGGIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

console.log("Before Acivity1 : \n" +activity1String);
activity1String = changeSecondLetterToO(activity1String)
console.log("After Acivity1 : \n" +activity1String);

// Activity 2
// While a while loop that generates a random number between 1 and 10 and prompts the user 
// to guess the number. If the user guesses correctly, print a congratutory message and exit 
// the loop. If the user guesses incorrectly give them a hint and prompt them to guess again


function guessNumber() {
    const target = Math.floor(Math.random() * 10) + 1; // generate random number between 1 and 10
    let guess = parseInt(prompt("Guess a number between 1 and 10:")); // prompt the user for a guess
  
    while (guess !== target) { // loop until the user guesses correctly
        if (guess > target) {
            guess = parseInt(prompt("Too high! Guess again:")); // provide a hint and prompt for another guess
        } else {
            guess = parseInt(prompt("Too low! Guess again:")); // provide a hint and prompt for another guess
        }
    }  
    alert("Congratulations! You guessed the number " + target + " correctly!"); // provide feedback on successful guess
}


// Activity 3
// Write a do while loop that prompts the user to enter a password until they enter the 
// correct password. The correct password is "password123"



let password = "password123";
do { 
} while (password !== prompt("Please enter your password:"));
alert("Correct password entered!");