//index.js
//
//--------------------------Git Bash-----------------------------------
//
//$ npm init -y
//
// Wrote to C:\Users\charl\Documents\b270\backend\s36\discussion\package.json:
// {
//   "name": "discussion",
//   "version": "1.0.0",
//   "description": "",
//   "main": "index.js",
//   "scripts": {
//     "test": "echo \"Error: no test specified\" && exit 1"
//   },
//   "keywords": [],
//   "author": "",
//   "license": "ISC"
// }
//
// $ npm install express mongoose
//
// npm notice created a lockfile as package-lock.json. You should commit this file.
// npm WARN notsup Unsupported engine for bson@5.2.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
// npm WARN notsup Not compatible with your version of node/npm: bson@5.2.0
// npm WARN notsup Unsupported engine for mongodb@5.3.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
// npm WARN notsup Not compatible with your version of node/npm: mongodb@5.3.0
// npm WARN discussion@1.0.0 No description
// npm WARN discussion@1.0.0 No repository field.
//
// + express@4.18.2
// + mongoose@7.1.0
// added 81 packages from 102 contributors and audited 81 packages in 8.47s
//
// 8 packages are looking for funding
//   run `npm fund` for details
//
// found 0 vulnerabilities
//
// -------------------------------create new file---------------------
//.gitignore (filename)
//node_modules (inside file)
//
//
// ----------------------------------
//	$ nodemon index.js

//-------------------------------------------------------------

// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

// This allows us to use all the routes defined in "taskRoute.js"
const taskRoutes = require("./routes/taskRoutes")

// Server setup
const app = express();
const port = 3001;

//Database connection
//mongodb+srv://admin:(PASSWORD)@zuitt.5e2pgug.mongodb.net/(COLLECTION NAME)?retryWrites=true&w=majority
//https://cloud.mongodb.com/ -> database -> connect
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5e2pgug.mongodb.net/b270-to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/tasks" route
app.use("/tasks", taskRoutes);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

//////////////////////////////////////////////////////////////////////////////////////////
//						    		A C T I V I T Y
//////////////////////////////////////////////////////////////////////////////////////////

// S36 Activity:
// 1. Create a route for getting a specific task.
// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
// 5. Create a route for changing the status of a task to "complete".
// 6. Create a controller function for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
// 9. Create a git repository named S31.
// 10. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 11. Add the link in Boodle.
