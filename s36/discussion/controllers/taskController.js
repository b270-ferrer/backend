// Controllers contain the functions and business logic of our Express application
// All the operations that it can do will be placed in this file
const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result
	})
}
module.exports.createTask = (requestBody) =>{
	// Creates a Task object based on the model Task
	let newTask = new Task ({
		// Sets the name property with the value recieved the client/ postman
		name: requestBody.name
	})

	return newTask.save().then ((task,error) =>{
		if (error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err) =>{
		if (err){
			console.log(err);
			return false;
		}else {
			return removedTask;
		
		}
	})
}


module.exports.updateTask = (taskId, newContent) =>{
	return Task.findById(taskId).then((result,err) =>{
		if (err){
			console.log(err);
			return false;
		}
		//Result of findByID willbe stored in the "result" parameter
		//Its "property" will br reassigned to the value of the "name" recieved from the request
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr) =>{
			if (saveErr){
				console.log(saveErr);
				return false;
			}else {
				return updatedTask;
		
			}
		})
	})
}


//////////////////////////////////////////////////////////////////////////////////////////
//						    		A C T I V I T Y
//////////////////////////////////////////////////////////////////////////////////////////

// getting a specific task
module.exports.getTasksById = (taskId) => {
	return Task.findById(taskId).then((task,err) =>{
		if (err){
			console.log(err);
			return false;
		}else{
			return task;
		}
	})
}

// update a task's result to complete
module.exports.updateTaskToComplete = (taskId) =>{
	return Task.findByIdAndUpdate(taskId).then((result,err) =>{
		if (err){
			console.log(err);
			return false;
		} else {
			result.status = "complete";
		}

		
		return result.save().then((completedTask, saveErr) =>{
			if (saveErr){
				console.log(saveErr);
				return false;
			}else {
				return completedTask;
		
			}
		})
	})
}












