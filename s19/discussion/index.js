 // console.log("hello");

// {SECTION] if. else if, and else


// Syntax
/*
	if (condition){
		statement
	}
*/

numA = -1
if (numA < 0){
	console.log ("Hello")
}


city = "New York"

if (city === "New York"){
	console.log("Welcome to New York City")
}

// else if statement

numH = 1;

if (numH < 0){
	console.log("Hello again!");
}else if (numH > 0){
	console.log ("world");
}


city = "Tokyo"
 
if (city === "New York"){
	console.log("Welcome to New York City");
}else if (city === "Tokyo"){
	console.log("Welcome to Tokyo City");
}


// else statement

city = "Manila"
 
if (city === "New York"){
	console.log("Welcome to New York City");
}else if (city === "Tokyo"){
	console.log("Welcome to Tokyo City");
}else{
	console.log("again");
}


//if, else if, else

function determineTyphonIntensity(windSpeed){
	if (windSpeed < 30) {
		return "Not atyphoon yet";
	} else if (windSpeed <= 61){
		return "Tropical depression detected";
	} else if (windSpeed > 61 && windSpeed <= 88){
		return "Tropical storm detected";
	} else if (windSpeed > 88 && windSpeed <= 117){
		return "Severe Tropical storm detected";
	} else {
		return "Typhon detected";
	}
}

message = determineTyphonIntensity(110);
console.log (message);

message = determineTyphonIntensity(120);
console.log (message);

// [Section] Truthy and Falsy

// Truthy Examples
if (true) {
	console.log ("Truthy :true");
} 

if (1) {
	console.log ("Truthy : 1");
} 

if ([]) {
	console.log ("Truthy: []");
} 


// Falsy Examples
if (!false) {
	console.log ("Falsy :false");
} 

if (!0) {
	console.log ("Falsy : 0");
} 

if (!undefined) {
	console.log ("Falsy: undefined");
} 


if (!"") {
	console.log ("Falsy : \"\"");
} 

if (!null) {
	console.log ("Falsy: null");
} 


// [Section] Conditional Tenary Operator
// SYNTAX
// 
// 		(expression) ? ifTrue : ifFalse;

/*
	Conditional Tenary operator takes in three operands
		1.condition
		2. expression to be executed if the condition is truthy
		3. expression to be executed if the condition is a falsy

*/
let ternaryResult = (1 ,10) ? true : false;
console.log ("Result of ternary operator: " +ternaryResult);



// 
let name ;

	function isOfLegalAge(){
		name = "John";
		return "You are of legalage limit";
	}

	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit";
	}

//"parseInt()"  converts string to Integer
	let age = parseInt(prompt("What is your age? "));
	console.log(age);

	let legalAge = (age > 17) ? isOfLegalAge() : isUnderAge();
	console.log ("Result of Ternary operation: " + legalAge + " , " + name);

// [section] switch
	// Syntax (expression){
	// 	case value1 : 
	// 		statement;
	// 		break;
	// 	case value3 : 
	// 		statement;
	// 		break;
	// 	case default : 
	// 		statement;			
	// }





	let day = prompt("What day of the weekis it today?");
	console.log(day);

	// the .toLowerCase() will change the input from the prompt into all lowercase
	switch (day.toLowerCase()){
	 	case "monday": 
	 		console.log ("The color of the day is red");
	 		break; 

	 	case "tuesday": 
	 		console.log ("The color of the day is orange");
	 		break; 

	 	case "wednesday": 
	 		console.log ("The color of the day is yellow");
	 		break; 

	 	case "thursday": 
	 		console.log ("The color of the day is green");
	 		break; 

	 	case "friday": 
	 		console.log ("The color of the day is blue");
	 		break; 

	 	case "saturday": 
	 		console.log ("The color of the day is indigo");
	 		break; 

	 	case "sunday": 
	 		console.log ("The color of the day is violet");
	 		break; 

	 	default:
	 		console.log ("Enter a valid day");	
	}

// [section] try-catch-finally

function showIntensityAlert(windSpeed){
	try{
		alarat(determineTyphonIntensity(windSpeed));
	}
	catch (error){

		console.log(typeof error);
		// .warn highlight log
		console.warn(error.message); 
		console.warn(error.name);
	}
	finally{
		alert ("Intensity update will show new alert.");
	}
}


showIntensityAlert(56);






















