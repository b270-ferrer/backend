// console.log("hello");


// Functions
	// Functions in JS are lines/blocks of codes that can 
	// tell our device/application to perform a certain
	// task when called or inoked

// Function Declarations
	// function statement - defines a function with a 
	// 					specific parameter


// Syntax:
	// function functionName(){
	// 		code block (statement)
	// }
	
	//  function keyword - used to define a JS function
	// functionName - functions are named to be ableto be 
	// 		used to be called later in the code
	
	// function block{} - contains the body of the function


function printName(){
	console.log("Hi!, my name is John")
};

// function invocation
printName();

//Uncaught ReferenceError: declaredFunction is not defined
 // declaredFunction();

// Function Declaration vs Function Expression

// 1. declared function can be hoisted as long as it is defined
 declaredFunction();

	function declaredFunction(){
		console.log("Hello world from declaredFunction")
	}

 declaredFunction();

 // 2. Function Expression
 	// A function can be stored in a variable
 	// A function expression is anonymous function
 	// assigned to a variableFunction


 //	let variableName = function(){
 // 	code block (statement)
 // }

//Uncaught ReferenceError: Cannot access 'variableFunction' before initialization
// variableFunction();

let n = 30; 
// you cannot hoist function expression
let variableFunction = function(){
	console.log("Hello again")
}

variableFunction();

// We invoke using its variable name ,not its function name
let funcExpression = function funcName(){
	console.log("Hello from the other side")
};

funcExpression();
// Uncaught ReferenceError: funcName is not defined
// funcName()

// Yu can reassign declared function nd function expression to a new anonymous function


declaredFunction = function(){
	console.log("upadted declaredFunction")
};


funcExpression= function(){
	console.log("upadted funcExpression")
}


declaredFunction();

funcExpression();


const constantFunct = function(){
	console.log("initialized with const");
}

constantFunct();
declaredFunction();

// this will result in error
// constantFunct = function(){
// 	console.log("cnnot be reassigned");
// }

// Function Scoping
/*
	Scope is the accessibility(visibility) of variables within our program

		JavaScript Variables has 3 types of scope
			1	local/block scope
			2	global scope
			3.	function scope

*/

	// 1. Local Scope
	{
		let localVar = " I am Groot"
		console.log(localVar)
	}

	// 2. Global Scope
		let globalVar = "I am Steve Rogers"
	
	console.log(globalVar)
	// console.log(localVar)

	// 3. Function Scope
		function showNames(){
			var functionVar = "Bruce Banner";
			const functionConst = "Hulk";
			let functionLet = "Thor";

			// ctrl + shift + D to duplicate line
			console.log(functionVar); 
			console.log(functionConst); 
			console.log(functionLet); 

		}

		showNames();
		// Uncaught ReferenceError: functionVar is not defined
			// console.log(functionVar); 
			// console.log(functionConst); 
			// console.log(functionLet); 


// Nested function

		function myNewFunction(){
			let name = 'jane';
			function nestedFunction(){
				let nestedName ='John';
				console.log(name);
			}
			nestedFunction();	

		}

		myNewFunction();

		let globalName = "Alexandro";

		function myNewFunction2(){
				let nameInside = 'Renz';
				console.log(globalName);
		}

		myNewFunction2();


// using alert()

	alert("Hello World"); // will run immidately rhen page loads


	function showSampleAlert(){
		alert("Hello, User!");
	}

	showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed")

// Using prompt()

	//returns an empty string when there is no input and null if cancelled
	let samplePrompt = prompt("Enter your name: ");
	console.log("Hello, " + samplePrompt);

	console.log(typeof samplePrompt);



	function printWelcomeMessage(){
		let firstName = prompt("Enter your first name :");
		let lastName = prompt("Enter your last name :");

		alert("Hello, " + firstName + " " +lastName);
		alert("Welcome to my page!")

	}
	printWelcomeMessage();


// function naming convension, 
// 1. function name should be descriptive
	function getCourses(){
		let courses = ["science 101", "math 101", "english 101"]
		console.log(courses)
	}
// 2. avoid generic name to avoid confusion within code

	function get(){
		let name = "jaime"
		console.log(name);
	}
// 3. avoid pointless and inappropriate fuunction name
	function foo(){
		console.log(2555);
	}
// 4. Name your function using camelCasing

	function getCarDetail(){
	// 	let courses = ["science 101", "math 101", "english 101"]
	// 	console.log(courses)
	}





