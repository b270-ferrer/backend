/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function userDetails(){
			let fullName 	= prompt("Enter your full name: "); 
			let age 		= prompt("Enter your age: "); 
			let location 	= prompt("Enter your location: "); 
		
			console.log( 
				  "Hello,  "	+ fullName 	+"\n"
				+ "You are " 	+ age 		+" years old\n"
				+ "You live in "+ location  +"\n"
			);				
		}
	userDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/
	let top5MusicalArtist 	= [	"The Weeknd","Taylor Swift","Bad Bunny","Miley Cyrus","Ed Sheeran."	];	
	printTop5MusicalArtist();	
	function printTop5MusicalArtist(){
		console.log(  "The top 5 Musical Artist are : \n" 
			+"1. " +top5MusicalArtist[0] +"\n"
			+"2. " +top5MusicalArtist[1] +"\n"
			+"3. " +top5MusicalArtist[2] +"\n"
			+"4. " +top5MusicalArtist[3] +"\n"
			+"5. " +top5MusicalArtist[4] +"\n"
		);				
	}
	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	let top5MoviesOfAllTime = [
		["Avatar","82%"],	
		["Avengers: Endgame","94%"],
		["Avatar: The Way of Water","76%"],
		["Titanic","88%"],
		["Star Wars: Episode VII","93%"]
	]

	printTop5MoviesOfAllTime();
	function printTop5MoviesOfAllTime(){
		console.log("The top 5 movies of all time and their Rotten Tomato Rating are : " 
			+"\n 1. " +top5MoviesOfAllTime[0][0] 
			+"\n Rotten Tomato Rating " +top5MoviesOfAllTime[0][1] 
			+"\n 2. " +top5MoviesOfAllTime[1][0]
			+"\n Rotten Tomato Rating " +top5MoviesOfAllTime[1][1] 
			+"\n 3. " +top5MoviesOfAllTime[2][0]
			+"\n Rotten Tomato Rating " +top5MoviesOfAllTime[2][1] 
			+"\n 4. " +top5MoviesOfAllTime[3][0]
			+"\n Rotten Tomato Rating " +top5MoviesOfAllTime[3][1] 
			+"\n 5. " +top5MoviesOfAllTime[4][0]
			+"\n Rotten Tomato Rating " +top5MoviesOfAllTime[4][1] 
		)
	}
	

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

 printUsers();
 /*let printFriends() = */function printUsers(){
 	alert("Hi! Please add the names of your friends.");
 	let friend1 = prompt("Enter your first friend's name:"); 
 	let friend2 = prompt("Enter your second friend's name:"); 
 	let friend3 = prompt("Enter your third friend's name:");

 	console.log("You are friends with:")
	console.log(friend1); 
 	console.log(friend2); 
 	console.log(friend3); 
 };


 // console.log(friend1);
 // console.log(friend2);