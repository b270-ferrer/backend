// Comparison Query Operatio
/*

	$gt(greater then)
	$



*/
// [sub-section] $gt / $gte Operator
/*
	$gt(greater than)
	$gte(greater than or equal to)

	- Allows us to find documents thst have field number
	value greater than or equal to a specific value 

	-Syntax:
		db.collectionName.find({	field: 	{$gt: value}	})
		db.collectionName.find({	field: 	{$gte: value}	})

*/

db.users.find({	age: {$gt:50}	});
db.users.find({	age: {$gte:50}	});

// [sub-section] $lt / $lte Operator
/*
	$lt(less than)
	$lte(less than or equal to)

	- Allows us to find documents thst have field number
	value less than or equal to a specific value 

	-Syntax:
		db.collectionName.find({	field: 	{$lt: value}	})
		db.collectionName.find({	field: 	{$lte: value}	})

*/

db.users.find({	age: {$lt:50}	});
db.users.find({	age: {$lte:50}	});

// [sub-section] $ne
/*
	$ne(not equal	

	- Allows us to find documents thst have field number
	value not equal to a specific value 

	-Syntax:
		db.collectionName.find({	field: 	{$ne: value}	})
		

*/

db.users.find({	age: {$ne:82}	});

// [sub-section] $in
/*
	$in(in)
	

	- Allows us to find documents with specific match criteria
	one field using different value

	-Syntax:
		db.collectionName.find({	field: 	{$in: value}	})		
		db.collectionName.find({	
			field: 	{	$in: value}	
		})	
	
*/

db.users.find({	lastName : {$in:["Hawking", "Doe"] }	});
db.users.find({	courses : {$in:["HTML", "Reaact"] }	});




// [section] logical Query operators

// [sub-section] $or Operator
/*
	allows us to find documents that match a single criteria
	from multiple provided search criteria

	syntax:
		db.collectionName.find ({	$or: [{fieldA: valueA}, {fieldB: valueB}]		})


*/
db.users.find ({ $or: [	{firstName:"Neil"}, {age:21}	]	});
db.users.find ({ $or: [	{firstName:"Neil"}, {age:{$gte:21}}	]	});


db.users.find ({ 
	$or: [	
		{	firstName:"Neil"		}, 
		{	age: {
				$gte:21
			}
		}	
	]	
});

// [sub-section] $and Operator
/*
	allows us to find documents matching criteria in a single field

	syntax:
		db.collectionName.find ({	
			$and: [
				{age: {$lt:70}}, 
				{age: {$gt:60}}
			]		
		})
		db.collectionName.find ({		$and: [{
		age: {$lt:70}}, {age: {$gt:60}		}]		})

*/
		db.users.find ({$and: [	{age: {$lt:70}}, {age: {$gt:60}}	]})
		db.users.find ({
			$and: [
				{age: {$lt:70}}, {age: {$gt:60}
			]
		})


//[section] Field Projection
/*
	retrieving documents are common operations that we do and by
	defaulr MongoDb queries return the whole document as a respose

	when dealing with complex data structures, there might be instances 
	when field are not useful for the query that we are trying to accomplish


*/

// [sub-section] inclusion
/*
	Allow us to include/add specific fields only when retrieving
	documents

	the value provided is 1 to denote the field is being included

	syntax:
		db.collectionName.find ({criteria}, {field:1})

*/
db.users.find ({ firstName:"Jane"}, {firstName:1, lastName:1,contact:1});

db.users.find (
	{ 
		firstName:"Jane"
	}, 
	{	
		firstName:1, lastName:1,contact:1
	}
);

// [sub-section] exclusion
/*
	Allow us to exclude/remove specific fields only when retrieving
	documents

	the value provided is 0 to denote the field is being included

	syntax:
		db.collectionName.find ({criteria}, {field:0})

*/
db.users.find ({ firstName:"Jane"}, {contact:0, department:0});

db.users.find (
	{ 
		firstName:"Jane"
	}, 
	{	
		contact:0,
		department:0
	}
);

// [sub-section] suppressing the ID field
/*
	Allow us to exclude the "_id" when retrieving
	documents

	the value provided is 0 to denote the field is being included

	syntax:
		db.collectionName.find ({criteria}, {_id:0})

*/

db.users.find ({ firstName:"Jane"}, {firstName:1, lastName:1, contact:1, _id:0});

db.users.find (
	{ 
		firstName:"Jane"
	}, 
	{	
		firstName:1, 
		lastName:1, 
		contact:1, 
		_id:0
	}
);

// [sub-section] returning specific fields in embedded documents


db.users.find ({ firstName:"Jane"}, {firstName:1, lastName:1, "contact.phone":1, _id:0});

db.users.find (
	{ 
		firstName:"Jane"
	}, 
	{	
		firstName:1, 
		lastName:1, 
		"contact.phone":1 ,
		_id:0
	}
);

// [sub-section] suppressing specific fields in embedded documents


db.users.find ({ firstName:"Jane"}, {"contact.phone":0});

db.users.find (
	{ 
		firstName:"Jane"
	}, 
	{	
		"contact.phone":0 		
	}
);


// [section] evaluation query operation

// [sub-section] $regex operation
/*
	allows us to find documents that match a specific string patern
	using regular expression

	syntax:
		db.collectionName.find({	field:  $regex:  'pattern', $options:
		'$optionValue'});

*/

// Case sensitive query
db.users.find ({ firstName: {$regex:'N'} });

db.users.find (
	{ 
		firstName: {	$regex:'N'	} 
	} 	
);

// Case insensitive query
db.users.find ({ firstName: {$regex:'j', $options:'i'} });

db.users.find (
	{ 
		firstName: {	
			$regex:'N', 
			$options:'i'	
		} 
	} 	
);