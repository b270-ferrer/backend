const express = require("express");
const router = express.Router();
const userController = require ("../controllers/userController");
const auth = require ("../auth")

// Route for checking if the user email already exist in the database
// invokes the checkEmailExist from thr controller with our databse
router.post ("/checkEmail", userController.checkEmailExists)


// Route for user registration
router.post("/register", userController.registerUser)


// Route for user authentication
router.post("/login", userController.loginUser)

// Route for user details
router.get("/details", auth.verify, userController.getProfile);


// Route for enrolling a user to a course
router.post("/enroll", auth.verify, userController.enroll);






module.exports = router;