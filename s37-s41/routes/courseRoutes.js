const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController");
const auth = require ("../auth")

// Route for creating a course
// router.post("/", courseController.addCourse)


////////////////////////////////////////////////////////////////////////////
//		ACTIVITY S39
///////////////////////////////////////////////////////////////////////////
// S39 Activity:
// 1. Refactor the course route to implement user authentication for the admin when creating a course.
// 2. Refactor the addCourse controller method to implement admin authentication for creating a course.
// 3. Push to git with the commit message of Add activity code - S39.
// 4. Add the link in Boodle.


router.post("/", auth.verify, courseController.addCourse)

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.getAllCourses);

// Route for retrieving all ACTIVE courses
router.get("/", courseController.getAllActive);

// Route for retrieving a speciifc courses
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending on the information provided in the url
router.get("/:courseId", courseController.getCourse);

// Route for updating a course
router.put("/:courseId", auth.verify, courseController.updateCourse);


////////////////////////////////////////////////////////////////////////////
//		ACTIVITY S40
///////////////////////////////////////////////////////////////////////////

    // 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
    // 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
    // 3. Process a PATCH request at the /courseId/archive route using postman to archive a course
    // 4. Push to git with the commit message of Add activity code - S40.
    // 5. Add the link in Boodle.

router.patch("/:courseId", auth.verify, courseController.archiveCourse);






module.exports = router;