const mongoose = require("mongoose");;

const courseSchema = new mongoose.Schema ({
	name:{
		type: String,
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]	
	},
	price:{
		type: Number,
		required: [true, "Price is required"]
	},
	slots:{
		type: Number,
		required: [true, "Slots is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		// the "new Date()" expression instantiate a new 
		//"date" tht stores the current data and time whever a course is create in the database
		default: new Date()
	},
	enrollees: [
		{
			userId:{
				type: String,
				required: [true, "User ID is required"]
			},
			isPaid:{
				type: Boolean,
				default:true
			},
			dateEnrolled:{
				type: Date,
				default:new Date()
			},		
		}
	]
});

module.exports = mongoose.model("Course", courseSchema);