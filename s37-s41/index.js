
			// charl@Sunshine MINGW64 ~/Documents/b270/backend/s37-s41 (master)
			// $ npm init -y
			// Wrote to C:\Users\charl\Documents\b270\backend\s37-s41\package.json:

			// {
			//   "name": "s37-s41",
			//   "version": "1.0.0",
			//   "description": "",
			//   "main": "index.js",
			//   "scripts": {
			//     "test": "echo \"Error: no test specified\" && exit 1"
			//   },
			//   "keywords": [],
			//   "author": "",
			//   "license": "ISC"
			// }



			// charl@Sunshine MINGW64 ~/Documents/b270/backend/s37-s41 (master)
			// $ touch index.js

			// charl@Sunshine MINGW64 ~/Documents/b270/backend/s37-s41 (master)
			// $ npm i express
			// npm notice created a lockfile as package-lock.json. You should commit this file.
			// npm WARN s37-s41@1.0.0 No description
			// npm WARN s37-s41@1.0.0 No repository field.

			// + express@4.18.2
			// added 57 packages from 42 contributors and audited 57 packages in 5.915s

			// 7 packages are looking for funding
			//   run `npm fund` for details

			// found 0 vulnerabilities


			// charl@Sunshine MINGW64 ~/Documents/b270/backend/s37-s41 (master)
			// $ npm i mongoose
			// npm WARN notsup Unsupported engine for mongodb@5.3.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
			// npm WARN notsup Not compatible with your version of node/npm: mongodb@5.3.0
			// npm WARN notsup Unsupported engine for bson@5.2.0: wanted: {"node":">=14.20.1"} (current: {"node":"14.18.0","npm":"6.14.15"})
			// npm WARN notsup Not compatible with your version of node/npm: bson@5.2.0
			// npm WARN s37-s41@1.0.0 No description
			// npm WARN s37-s41@1.0.0 No repository field.

			// + mongoose@7.1.0
			// added 24 packages from 64 contributors and audited 81 packages in 3.139s

			// 8 packages are looking for funding
			//   run `npm fund` for details

			// found 0 vulnerabilities
////////////////////////////////////
// bcrypt
/////////////////////////////////////
// $ npm install bcrypt

// > bcrypt@5.1.0 install C:\Users\charl\Documents\b270\backend\s37-s41\node_modules\bcrypt
// > node-pre-gyp install --fallback-to-build

// [bcrypt] Success: "C:\Users\charl\Documents\b270\backend\s37-s41\node_modules\bcrypt\lib\binding\napi-v3\bcrypt_lib.node" is installed via remote
// npm WARN s37-s41@1.0.0 No description
// npm WARN s37-s41@1.0.0 No repository field.

// + bcrypt@5.1.0
// added 60 packages from 135 contributors and audited 141 packages in 10.009s

// 11 packages are looking for funding
//   run `npm fund` for details


/////////////////////////////////////
//   jsaon web token
//////////////////////////////////////
// $ npm install jsonwebtoken
// npm WARN s37-s41@1.0.0 No description
// npm WARN s37-s41@1.0.0 No repository field.
//
// + jsonwebtoken@9.0.0
// added 7 packages from 7 contributors and audited 148 packages in 3.545s
//
// 11 packages are looking for funding
//   run `npm fund` for details
//
// found 0 vulnerabilities


///////////////////////////////////////////////
///     CORS Cross Origin Resource Sharing
////////////////////////////////////////////
// $ npm install cors
// npm WARN s37-s41@1.0.0 No description
// npm WARN s37-s41@1.0.0 No repository field.
//
// + cors@2.8.5
// added 1 package from 1 contributor and audited 149 packages in 3.515s
//
// 11 packages are looking for funding
//   run `npm fund` for details
//
// found 0 vulnerabilities





// Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");


//Allow our backend application to be available to our frontend
const cors = require ("cors")


//

const userRoutes = require ("./routes/userRoutes");
const courseRoutes = require ("./routes/courseRoutes");


const app = express();


//Database connection
//mongodb+srv://admin:(PASSWORD)@zuitt.5e2pgug.mongodb.net/(COLLECTION NAME)?retryWrites=true&w=majority
//https://cloud.mongodb.com/ -> database -> connect
mongoose.connect("mongodb+srv://admin:admin123@zuitt.5e2pgug.mongodb.net/b270_booking?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);


//Set notification for database connection
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", ()=> console.log("We are connected to cloud databse"))

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
//defines the /user to be included for all user routes
app.use("/users", userRoutes)
//defines the /courses to be included for all user routes
app.use("/courses", courseRoutes)
//
app.use(cors());

// Will use the defined port number for the application whenever an environment variable is 
//available OR will use port 4000 if none is defined

//this syntax will allows flexibility when using application locally or as a hosted application 

app.listen (process.env.PORT || 4000, () =>{
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
})







