let http = require('http');
let port = 4000;

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('Hello World!');
}).listen(port);

console.log (`Server is live at at port ${port}`);
