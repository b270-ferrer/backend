//alert(`Hi, JSON!`);


//[section] JSON Objects
/*
    -JSON stands for JavaScript Object Notation
    -JSON is used in other programming language
    -JSON is used for serializing diffent data type into bytos
    -Serialization is the process of converting data into a series of bytes for easier transmission
    -Syntax:
        {
            "propertyA": "valuaA",
            "propertyB": "valueB"
        }

*/



//JSON Objects
// {
//     "city":"Quezon City",
//     "province":"Metro Manila",
//     "county": "Philippines"
// }


//JSON Arrays
// "cities": [
//     {
//         "city":"Quezon City",
//         "province":"Metro Manila",
//         "county": "Philippines"
//     }
//         ,
//     {
//         "city":"Manila City",
//         "province":"Metro Manila",
//         "county": "Philippines"
//     }
//         ,
//     {
//         "city":"Makati City",
//         "province":"Metro Manila",
//         "county": "Philippines"
//     }
// ]



//[section] JSON Methods
// the JSON object contains methods for parsing and converting data into stringified metthod

let batchesArr = [
    {
        "batchName": "Batch 270"
    }   
        ,  
    {
        "batchName": "Batch 271"
    }

]

console.log(batchesArr);
// The "stringify" method is used to convert JS objects to string
console.log(`Resulr of "stringify method"`);
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify(
                {
                    name: "John",
                    age: 31,
                    address:{
                        city: "Manila",
                        country: "Philippines"
                    }

                }
            )

console.log(data);

// user details
// let firstName = prompt(`What is your first name?  `);
// let lastName = prompt(`What is your last name?  `);
// let age = prompt(`What is your age?  `);
// let address = {
//     city: prompt(`What city do you live in?  `),
//     country: prompt(`country do you belong to?  `)
// }

// let otherData = JSON.stringify({
//     firstName: firstName ,
//     lastName:lastName,
//     age:age,
//     address:address
// });


// console.log(otherData);


//[section] Converting Stringified JSON into JS Object

let batchesJSON = `[
    {
        "batchesName": "Batch 270"
    }   ,
    {
        "batchesName": "Batch 271"
    }   
]`;
// let batchesJSON = `[
//     {
//         batchesName: "Batch 270"//error
//     }   ,
//     {
//         batchesName: "Batch 271"//error
//     }   
// ]`;


console.log("Result from parse method: ");
console.log (JSON.parse(batchesJSON))


let stringifiedObject = `{
    "name": "John",
    "age": "31",
    "address": {
         "city": "Manila",
         "country": "Philippines"
    }
}`;


console.log (stringifiedObject)
console.log (JSON.parse(stringifiedObject))

































































