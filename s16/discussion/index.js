// console.log("Hello World");

// Arithmitic Operator
	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log(sum);

	let difference = x - y;
	console.log(difference);

	let product = x * y;
	console.log(product);

	let quotient = y / x;
	console.log(quotient);

	let remainder = x % y; //modulo
	console.log(remainder);

// Assignment Operator

	// Basic Arithmitic Operator (=)
	let assignmentNumber = 8; //use camelCassing

	// Addition Arithmitic Operator (+=)
	// assignmentNumber = assignmentNumber +2; //use camelCassing
	// console.log(assignmentNumber );

	//short hand for assignmentNumber = assignmentNumber +2
	assignmentNumber += 2;
	console.log("Result of assignment operator(+=) "+assignmentNumber );

	assignmentNumber -= 2;
	console.log("Result of assignment operator(-=) "+assignmentNumber );

	assignmentNumber *= 2;
	console.log("Result of assignment operator(*=) "+assignmentNumber );

	assignmentNumber /= 2;
	console.log("Result of assignment operator(/=) "+assignmentNumber );

	assignmentNumber %= 2;
	console.log("Result of assignment operator(%=) "+assignmentNumber );

// Multiple Operators
	// M D A S

	let number = 1 + 2 - 3 * 4 / 5;
	//The opertion were done in the following order
	// 1. 	3 * 4 = 12
	// 2.	12 / 5 = 2.4
	// 3.	1 + 2 = 3
	// 4. 	3 - 2.4 = 0.6
	console.log("Result of MDAS operation"+number);

	// PEMDAS
	let pendas = 1 + (2 - 3) * (4 / 5);
	//The opertion were done in the following order
	// 1. 	4 / 5 = 0.8
	// 2.	2 - 3 = -1
	// 3.	-1 * 0.8 = -0.8
	// 4. 	1 + -0.8 = 0.2
	console.log("Result of PEMDAS operation"+pendas);

// TYpe Coercion
	let numA = "10";
	let numB = 12;
	let coercion = numA + numB;
	console.log(coercion)

	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	let noCoercion = numC + numD;
	console.log(noCoercion)

	console.log(typeof noCoercion);

	// the value of true is 1
	let numE = true +1;
	console.log(numE);


	// the value of false is 0
	let numF = false +1;
	console.log(numF);

// Comprison Operator
	let juan = "juan";

	console.log ("Equality Operator (==)");
	// Equality Operator (==)
	console.log (1 == 1);		//true
	console.log (1 == 2);			//false
	console.log (1 == "1"); 		//true
	console.log (0 == false);		//true
	console.log ("juan" == "juan"); //true
	console.log (juan == "juan");	//true


	// Inequality Operator (!=)
	console.log ("Inrquality Operator (!=)");
	console.log (1 != 1);			//false
	console.log (1 != 2);			//true
	console.log (1 != "1"); 		//false
	console.log (0 != false);		//false
	console.log ("juan" != "juan"); //false
	console.log (juan != "juan");	//false


	console.log ("Strict Equality Operator (===)");
	// Strict Equality Operator (===) checks data type
	console.log (1 === 1);			//true
	console.log (1 === 2);			//false
	console.log (1 === "1"); 		//false
	console.log (0 === false);		//false
	console.log ("juan" === "juan");//true
	console.log (juan === "juan");	//true


	console.log ("Strict Inequality Operator (===)");
	// Strict Inequality Operator (===) checks data type
	console.log (1 !== 1);			//false
	console.log (1 !== 2);			//true
	console.log (1 !== "1"); 		//true
	console.log (0 !== false);		//true
	console.log ("juan" !== "juan");//false
	console.log (juan !== "juan");	//false

	console.log ("Relational Operators");
// Relational Operators

	let a = 50;
	let b = 65;

	// Greater than Operator
	let isGreaterThan = a > b;
	console.log(isGreaterThan);		//false

	// Less than Operator
	let isLessThan = a < b;;
	console.log(isLessThan);		//true

	// Greater than or Equal Operator
	let isGreaterOrEqual = a >= b;
	console.log(isGreaterOrEqual);		//false

	// Less than or Equal Operator
	let isLessOrEqual = a <= b;
	console.log(isLessOrEqual);		//true


	console.log ("Logical Operator")
// Logical Operator
	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&&)
	// returns true if both operators  are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log ("Result of logical AND operations :" +allRequirementsMet);

	// Logical OR operator (||)
	// returns true if one operator is true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log ("Result of logical OR operations :" +someRequirementsMet);

	// Logical NOT operator (||)
	// returns opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log ("Result of logical NOR operations :" +someRequirementsNotMet);

// Increment and Decrement
	let z = 1;

	// Pre-Increment
	// the value of 'z' is incremented before returning a value
	let increment = ++z;
	console.log ("Result of pre-increment "+increment); // 2
	console.log ("Result of pre-increment "+z);			// 2

	// Post-Increment
	// the value of 'z' is returned before incrementing
	increment = z++;
	console.log ("Result of post-increment "+increment); // 2
	console.log ("Result of post-increment "+z);		 // 3

	// Pre-Decrement
	// the value of 'z' is incremented before returning a value
	let decrement = --z;
	console.log ("Result of pre-decrement "+decrement); // 2
	console.log ("Result of pre-decrement "+z);			// 2

	// Post-Decrement
	// the value of 'z' is returned before incrementing
	decrement = z--;
	console.log ("Result of post-decrement "+decrement); // 2
	console.log ("Result of post-decrement "+z);		 // 1

// NOte
	// sample=+2 is equivalent to sample = (+2)
	let sample =+2 
	console.log (sample)

	// sample=-2 is equivalent to sample = (-2)
	sample =-2 
	console.log (sample)














